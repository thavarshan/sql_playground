-- Average value of a numeric column.
SELECT AVG(gpa) FROM tests.students;

-- Average value of a numeric column as aliased column.
SELECT AVG(age) as avg_age FROM tests.students;

-- Count available entries in students table.
SELECT COUNT(id) as num_of_students FROM tests.students;

-- Get the smallest value of the selected column.
SELECT MIN(age) FROM tests.students;

-- Get the largest value of the selected column.
SELECT MAX(age) FROM tests.students;
