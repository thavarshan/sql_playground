-- Method 1: Insert values with column names.
INSERT INTO tests.courses (id, name, duration, payment) VALUES ('C001', 'Computer Architecture', 18, 20000000);
INSERT INTO tests.students (id, name, address, age, gpa, course_id) VALUES ('HND/COM/95/15', 'Thavarshan', '59, Martin Road, Jaffna', 25, 3.38, 'C001');

-- Method 2: Insert values without column names.
INSERT INTO tests.courses VALUES ('C002', 'System Analysis', 16, 30000000);
INSERT INTO tests.students VALUES ('HND/COM/95/16', 'James', '59, Martin Road, Jaffna', 25, 3.38, 'C002');
