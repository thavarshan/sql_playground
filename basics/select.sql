-- Get only 'id', 'name' columns from 'students' table.
SELECT id, name FROM tests.students;

-- Get all columns from 'students' table.
SELECT * FROM tests.students;
