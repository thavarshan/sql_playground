-- Create 'courses' table.
CREATE TABLE courses (
	id NVARCHAR(50) NOT NULL PRIMARY KEY,
	name NVARCHAR(100),
	duration INT,
	payment FLOAT
);

-- Create 'students' table.
CREATE TABLE students (
	id NVARCHAR(50) NOT NULL PRIMARY KEY,
	name NVARCHAR(100),
	address NVARCHAR(250),
	age INT,
	gpa FLOAT,
	course_id NVARCHAR(50),
	FOREIGN KEY (course_id) REFERENCES courses(id)
);
