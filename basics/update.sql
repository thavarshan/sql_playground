-- Update given row column with given data.
UPDATE tests.students SET age = 26 WHERE name = 'Thavarshan';

-- Update given row column(s) with given data.
UPDATE tests.students SET age = 26, address = '44, Post Office Road, Trincomalee' WHERE name = 'Thavarshan';
