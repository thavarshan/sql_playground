-- Search value under the given condition (record which name matches given).
SELECT name, address FROM tests.students WHERE name = 'Thavarshan';

-- Get all column details of row in 'students' table where name matches given name.
SELECT * FROM tests.students WHERE name = 'Thavarshan';

-- Get all students who have GPA more than 3.5.
SELECT * FROM tests.students WHERE gpa >= 3.5;
